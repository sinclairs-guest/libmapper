/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class mapper_Map */

#ifndef _Included_mapper_Map
#define _Included_mapper_Map
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     mapper_Map
 * Method:    mapperMapSrcSlotPtr
 * Signature: (JI)J
 */
JNIEXPORT jlong JNICALL Java_mapper_Map_mapperMapSrcSlotPtr
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     mapper_Map
 * Method:    mapperMapDstSlotPtr
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_mapper_Map_mapperMapDstSlotPtr
  (JNIEnv *, jobject, jlong);

/*
 * Class:     mapper_Map
 * Method:    mapperMapNew
 * Signature: ([Lmapper/Signal;Lmapper/Signal;)J
 */
JNIEXPORT jlong JNICALL Java_mapper_Map_mapperMapNew
  (JNIEnv *, jobject, jobjectArray, jobject);

/*
 * Class:     mapper_Map
 * Method:    refresh
 * Signature: ()Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_refresh
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    numProperties
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_mapper_Map_numProperties
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    property
 * Signature: (Ljava/lang/String;)Lmapper/Value;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_property__Ljava_lang_String_2
  (JNIEnv *, jobject, jstring);

/*
 * Class:     mapper_Map
 * Method:    property
 * Signature: (I)Lmapper/Property;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_property__I
  (JNIEnv *, jobject, jint);

/*
 * Class:     mapper_Map
 * Method:    setProperty
 * Signature: (Ljava/lang/String;Lmapper/Value;Z)Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_setProperty
  (JNIEnv *, jobject, jstring, jobject, jboolean);

/*
 * Class:     mapper_Map
 * Method:    removeProperty
 * Signature: (Ljava/lang/String;)Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_removeProperty
  (JNIEnv *, jobject, jstring);

/*
 * Class:     mapper_Map
 * Method:    clearStagedProperties
 * Signature: ()Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_clearStagedProperties
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    push
 * Signature: ()Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_push
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    expression
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_mapper_Map_expression
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    setExpression
 * Signature: (Ljava/lang/String;)Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_setExpression
  (JNIEnv *, jobject, jstring);

/*
 * Class:     mapper_Map
 * Method:    id
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_mapper_Map_id
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    isLocal
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_mapper_Map_isLocal
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    mapperMapMode
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_mapper_Map_mapperMapMode
  (JNIEnv *, jobject, jlong);

/*
 * Class:     mapper_Map
 * Method:    mapperMapSetMode
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_mapper_Map_mapperMapSetMode
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     mapper_Map
 * Method:    muted
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_mapper_Map_muted
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    setMuted
 * Signature: (Z)Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_setMuted
  (JNIEnv *, jobject, jboolean);

/*
 * Class:     mapper_Map
 * Method:    mapperMapProcessLoc
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_mapper_Map_mapperMapProcessLoc
  (JNIEnv *, jobject, jlong);

/*
 * Class:     mapper_Map
 * Method:    mapperMapSetProcessLoc
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_mapper_Map_mapperMapSetProcessLoc
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     mapper_Map
 * Method:    scopes
 * Signature: ()Lmapper/device/Query;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_scopes
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map
 * Method:    addScope
 * Signature: (Lmapper/Device;)Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_addScope
  (JNIEnv *, jobject, jobject);

/*
 * Class:     mapper_Map
 * Method:    removeScope
 * Signature: (Lmapper/Device;)Lmapper/Map;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_removeScope
  (JNIEnv *, jobject, jobject);

/*
 * Class:     mapper_Map
 * Method:    mapperMapNumSlots
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL Java_mapper_Map_mapperMapNumSlots
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     mapper_Map
 * Method:    ready
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_mapper_Map_ready
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
